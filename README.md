# Сборка инсталляции Wordpress-сайта

### Установка

1. Склонировать репозиторий
2. Запустить composer install. Эта команда загрузит Wordpress, положив его core-файлы в папку public/wp, а контент (темы, плагины, аплоады) в папку public/content. Кроме того, загрузит необходимые плагины и темы, разработанные сторонними вендорами. Свои плагины мы загружаем из репозитория напрямую
3. Если WP не создаст файл .htaccess в папке public, сделать это вручную. Стандартное содержимое: https://codex.wordpress.org/htaccess
4. На продакшн сервере скопировать файл production-config.sample.php в корне инсталляции в файл production-config.php и настроить соединение с базой данных, а также «соль». На локальном дев-сервере – скопировать файл local-config.sample.php в корне инсталляции в файл local-config.php и так же настроить.
  

### Разработка
- Корневая папка сайта теперь — public
- В .gitignore указаны пути к папкам/файлам, которые мы не отправляем в репозиторий. Это прежде всего файлы с секретными данными, core-файлы Wordpress и сторонние темы/плагины (они обновляются/загружаются с помощью composer)
- 

### Справка
- Идеи об использовании Composer + Git, а также отделения core файлов WP от остального контента и вынесении на уровень выше файлов конфигурации почерпнуты в статьях по этим ссылкам: https://deliciousbrains.com/storing-wordpress-in-git/ https://deliciousbrains.com/how-why-install-wordpress-core-subdirectory/ https://www.pmg.com/blog/composer-and-wordpress/
- Зеркало репозиториев Wordpress для Composer с плагинами и темами — https://wpackagist.org/